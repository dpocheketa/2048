var cellCount = 4;
var field = {

		container : document.getElementById("field"),

		elements : [],

		createEmptyCell : function(){
			var cell = document.createElement("div");

				cell.setAttribute("class", "cell");

				this.container.appendChild(cell);
		},

		init : function(){
			for (var i = 0; i < cellCount*cellCount; i++) {
				field.createEmptyCell();
			};
			for (var i = 0; i < cellCount; i++) {
				this.elements[i] = [];
			};
			this.createElem({value:2,position:{x:0,y:0}});
		},

		createElem : function(newElem){
			var elem = new Element(newElem.value, newElem.position);
			var domElem = document.createElement("div");

				domElem.setAttribute("class", "elem");
				domElem.style.top = elem.cssPosition.top + "px";
				domElem.style.left = elem.cssPosition.left + "px";
				domElem.innerHTML = elem.value;
				elem.domElem = domElem;

			this.container.appendChild(domElem);
		},

		move: function(dir){
			console.log(dir);
			this.render(this.elements);
		},

		render: function(arr){
			console.log(arr);
			for (var i = 0; i < arr.length; i++) {
				for (var j = 0; j < arr[i].length; j++) {
					this.createElem(arr[i][j]);
				};
			};
		}
		
	};


field.init();

document.addEventListener("keyup", function(e){
	var direction = false;
	switch (e.keyCode) {
		case 37: direction = "left"; 
			break;
		case 38: direction = "top"; 
			break;
		case 39: direction = "right"; 
			break;
		case 40: direction = "bottom"; 
			break;
		default: direction = false;
	}
	field.move(direction);
});

function Element(value, position){
	this.value = value;
	this.position = {
		x: position.x,
		y: position.y
	}

	this.cssPosition = {
		left : position.x*120 + 2 + 4 + 1 + 2,
		top : position.y*120 + 2 + 4 + 1 + 2
	};
};